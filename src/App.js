import logo from './logo.svg';
import './App.css';
import { useState } from 'react';

function App() {
  // you can have multiple useState hooks, e.g. for each individual property
  const [ firstName, setFirstName ] = useState("");
  const [ lastName, setLastName ] = useState("");

  function handleChange(event) {
     // note that we spread previous state
  event.target.name === 'firstName' ? setFirstName(event.target.value) : setLastName(event.target.value);
  }

  
  return (
    <div>
      <div>Welcome, {firstName + " " + lastName}</div>
      <div>
        <label>First name:</label>
        <input
          value={firstName}
          name="firstName"
          onChange={handleChange}
        />
      </div>
      <div>
        <label>Last name:</label>
        <input
          value={lastName}
          name="lastName"
          onChange={handleChange}
        />
      </div>
   </div>
  );
}

export default App;
